\subsection{ConBiNE}
\texttt{GOLEMlite} does not, at the time of writing, have any infrastructure agents included in the framework, out of the box. However, the sample application shipped with \\\texttt{GOLEMlite} \textit{does} make use of the various agent classifications.

\texttt{ConBiNe} is a multi-agent system representing an open market \cite{lam2005trading}, where each buyer and each seller can negotiate with many opponents simultaneously. The purpose of creating this multi-agent system is to compare and analyse different concurrent negotiation strategies to determine which was the best in this type of domain. The agent environment is represented by the market place, and there exist three types of agent in the application:

\begin{itemize}
    \item \texttt{MarketAgent}
    \item \texttt{MarketBroker}
    \item \texttt{MarketController}
\end{itemize}

\texttt{MarketAgent} is the generalisation of buyer and seller agents. These agents are created (as either buyer or seller), initialised with a particular negotiation strategy, then "made present" (registered) within the \texttt{Container} (the market place). When they are started (each agent in \texttt{GOLEMlite} has its own execution thread, which can be controlled independently), they are able to interact with other agents in the market place. When they have finished negotiating (they achieve their goal, their deadline runs out, or they are removed by the \texttt{MarketController}), they exit the market place, and they are destroyed.

\texttt{MarketAgent} is classed as an application agent. It is domain dependent, has a local view of the environment, and is the focus of the application - end-users are interested in the strategies used by these agents (and in the case of ConBiNe, the end-users can choose which strategy these agents adopt).

\texttt{MarketBroker} is an information broker. Its purpose is to keep track of the \\\texttt{MarketAgents} in the market place, and notify buyers about sellers that they might be interested in. The interest is based on a rudimentary matching scheme, where a buyer and seller match if the buyer is looking to buy product \textit{A} and the seller is looking to sell instances of product \texttt{A}.

\texttt{MarketBroker} is a hybrid agent. Its purpose is to provide a knowledge broking service to \texttt{MarketAgents}. It is domain dependent, and although it doesn't use the global view of the environment which it has access to, the application could be reimplemented in such a way so that it could (currently, when \texttt{MarketAgents} join the market place, they announce their ID and interest via a broadcast speech-act). End-users should not be overly concerned with the \texttt{MarketBroker} - as long as it functions (that is, it notifies buyers about relevant sellers) that is sufficient. The \texttt{MarketBroker} strategy is not able to be modified by end-users as the application developer has decided it is not a focus of the application.

\texttt{MarketController} is the agent which manages the simulation. It creates, adds and starts the \texttt{MarketBroker}, and is responsible for adding and removing \\\texttt{MarketAgents} during the experiment to simulate an open market place.

The \texttt{MarketController} is an agent which is closest to fitting the moniker of infrastructure agent, in this example. It is constructed by the application developer, and is domain dependent, but does not interact with \texttt{MarketAgents} to provide them a service or affect their goals; it does send requests to \texttt{MarketAgents} to leave the market place, rather than removing them directly, but this is to guarantee stability of the system rather than to provide \texttt{MarketAgents} with the opportunity to refuse or continue (it is assumed when they receive such a request, they will comply by terminating all negotiations and then indicate it is safe for them to be removed). The \texttt{MarketController} attempts to balance system resources by utilising them effectively - it creates new agents using threads from a pool to avoid excessive spawning of new threads to save memory, and can destroy all agents, including itself when the experiment is finished. The \texttt{MarketController} is able to access the container history - it saves the history at the end of each run to a file, so that it can be processed later.


\subsection{JADE}
The three main infrastructure agents which exist in the \texttt{JADE} platform are the \texttt{AMS} (Agent Management Service), the \texttt{DF} (Directory Facilitator) and the \texttt{Sniffer} (detects speech-acts at runtime).

JADE allows containers to be run on machines across a network, where a group of containers constitute a \textit{platform}. In a single platform, there must be exactly one \textit{main-container} which must be started first (other containers will register with this), and it is in this container that the \texttt{AMS} and \texttt{DF} agents must be run.

The \texttt{AMS} agent is the infrastructure agent which registers new agents onto the platform when they are created. Once an agent has been registered with the \texttt{AMS}, it can communicate via speech-acts, with any other agent in the platform. 

The \texttt{DF} agent is a yellow-pages service to which agents can publish their capabilities; application agents can query the \texttt{DF} (with partial search terms) and the \texttt{DF} will return a list of agents in the platform that match the search term. The \texttt{DF} is a hybrid agent created by the platform developer - it provides a direct service to application agents, its behaviour cannot be modified by the end-user, and it is unimportant to the end-user unless it stops working.

The \texttt{Sniffer} agent is an infrastructure agent that detects speech-acts midflight and constructs UML-like diagrams showing communication between (selected) agents in the platform. It is not an application agent, as it doesn't interact at all with other application agents, nor does it have any particular goal within a domain-dependent setting. As it doesn't provide any service to application agents either, it isn't a hybrid agent, but an infrastructure agent.
