In \texttt{GOLEMlite}, we defined user roles \cite{ataul2013}: platform developer, application developer, and end-user. The platform developer is the developer working on the framework, \texttt{GOLEMlite}. The application developer is the individual making multi-agent systems using the platform/framework. The end-user is the person for whom the application was built. There are no constraints on which roles (or combinations thereof) a single individual may hold. These roles of use when designing the framework - the platform developer must plan for the application developer's goals, and the application developer must plan for the end-user's goals.

We propose the following three classifications for agents: \textit{application agents}, \textit{infrastructure agents} and \textit{hybrid agents}.

\subsection{Application Agents}
\label{sec:app-agents}
Application agents represent the agents which are most entrenched within the environment; they are unaware of anything outside of the environment as specified by the application domain. These agents have the most limited view of the environment - although all the agents are situated in the same multi-agent system, they have varying views of the environment \cite{gouaich2005towards}.

Within \texttt{GOLEMlite}, these are the agents for which the agent environment is seen through the \texttt{Environment} interface, a limited view of the world in which they can perform actions. They are unaware of the underlying framework, and cannot see the the remainder of the \texttt{Container} as per Fig. \ref{fig:app-agents}.

Application agents are the agents whose behaviour the end-user is concerned with.


%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{fig/app-agents.pdf} 
        \caption{Application Agents}
        \label{fig:app-agents}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%




% --------------------------------------------------------------------------- %

\subsection{Infrastructure Agents}
\label{sec:infrastructure-agents}
We define infrastructure agents as agents that act on behalf of the environment. Their goal is to help ensure the continued health of the underlying system, that is, helping to ensure that the system meets its intended design specification. This may include acting on behalf of the platform developer to optimise the use of the platform (system resources, where these may be given to the platform for example) when running multiple applications simultaneously. Infrastructure agents have a unified view of the \texttt{Environment}/\texttt{Container} (Fig. \ref{fig:inf-agents}).

Infrastructure agents are developed by platform developers and application developers only - end users should not be able to supply a custom strategy for these agents; if it’s the case that it’s appropriate for the end user to be able to do this, then the agents in question are likely not infrastructure agents. In the case of infrastructure agents developed by the platform developer, it maybe be that the end-user is not even aware of their existence.

%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
    \begin{center}
        \includegraphics[width=\linewidth]{fig/inf-agents.pdf} 
        \caption{Infrastructure Agents}
        \label{fig:inf-agents}
    \end{center}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%

Application developers should use infrastructure agents to help achieve their goal (make working applications). This involves assigning the infrastructure agents responsibility for certain tasks, like collecting a particular metric, or entrusting them to coordinate application-oriented agents to their goals, like a knowledge broker agent (see \ref{sec:hybrid-agents}). Infrastructure agents are not biased to any particular application agent. E.g. an infrastructure agent responsible for guaranteeing the delivery of private messages in the environment should consistently ascribe the property of non-repudiation to every successful delivery for both the sender and recipient.

Infrastructure agents in \texttt{GOLEMlite} have access to the \texttt{ContainerHistory} and the register of \texttt{Entities} in the \texttt{Container}. Infrastructure agents may be domain independent, for example agents responsible for balancing the available system resources to various \texttt{Containers} running on the same platform.

% --------------------------------------------------------------------------- %

\subsection{Hybrid Agents}
\label{sec:hybrid-agents}
A hybrid agent is an infrastructure agent that exists primarily within the environment, and interacts with application-oriented agents. Again, their strategies are fixed by the application-developer (typically not the platform-developer unless the agent is providing some generic service) because they are typically domain-dependent; their main function is to provide services to application-oriented agents, like an information broker that can respond to domain-specific queries, such as "are there any agents nearby that can mend my boots?", for instance.

This classification exists to identify the agents which perform a required role, or provide an essential service to other agents in the environment, but are themselves not the interest of the application - for example, in an application where agents are separated into teams, and tasked with performing some objective, there may be a hybrid agent which gives directives to the two teams (e.g. "collect as many red balls as possible in 30 seconds") - in this instance, the end-user is concerned with the performance of the agents on the two teams, and the actions of the hybrid agent (so long as it performs its duties) are of no consequence.
